# Cmake For Botan


This is `CMakeLists.txt` for the botan C++ crypto library.

It generates amalgamated build of botan (`botan_all.h` and `botan_all.cpp`) at configure time, and creates static library target named `botan` 

## Usage
Download the `CMakeLists.txt` and put it in a directory alongside a botan source archive, like that:
```
	extlibs/
		botan/
			CMakeLists.txt
			botan-2.15.0.tar.gz
```

Then you have to set variable `BOTAN_ARCHIVE` to the archive name and `BOTAN_MODULES_LIST` to list of modules you'd like to be included in amalgamated build. Add the `CMakeLists.txt` into your cmake build, for example: 
```
set(BOTAN_ARCHIVE botan-2.15.0.tar.gz)
set(BOTAN_MODULES_LIST
    chacha20poly1305
    blake2
    system_rng
)
if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
    # add avx optimizations when building for the x86_64
    list(APPEND BOTAN_MODULES_LIST chacha_avx2)
endif()
add_subdirectory("extlibs/botan")
```
And use the target `botan` as dependency
```
target_link_libraries(ma-exe-or-lib PRIVATE botan)
```


